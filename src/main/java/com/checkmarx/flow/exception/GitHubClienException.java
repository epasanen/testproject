package com.checkmarx.flow.exception;


public class GitHubClienException extends MachinaException {
    public GitHubClienException() {
    }

    public GitHubClienException(String message) {
        super(message);
    }
}
