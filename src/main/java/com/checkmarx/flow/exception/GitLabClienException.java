package com.checkmarx.flow.exception;


public class GitLabClienException extends MachinaException {
    public GitLabClienException() {
    }

    public GitLabClienException(String message) {
        super(message);
    }
}
