package com.checkmarx.flow.exception;


public class BitBucketClienException extends MachinaException {
    public BitBucketClienException() {
    }

    public BitBucketClienException(String message) {
        super(message);
    }
}
